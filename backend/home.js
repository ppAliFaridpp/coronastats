const express = require('express');
const axios = require('axios');

const router = express.Router();

router.get('/', function(req, res) {
	axios.get('http://covid19api.xapix.io/v2/locations').then(function(response) {
		return res.render('pages/home', {countries: response.data.locations});
	}).catch(function(errors) {
		return res.status(500).json({message: 'Internal error'});
	});
});

module.exports = router;